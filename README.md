![Redenge logo](http://redenge.cz/wp-content/uploads/LOGO-REDENGE-internet-solutions.png)
# Redenge Engine
Obsahuje základní objekty pro základ každého e-commerce řešení.

## Požadavky
- verze PHP 5.6 a vyšší
- balíček nette/nette ~2.4

## Instalace
Nejlepší možnost, jak nainstalovat balíček je přes composer  [Composer](http://getcomposer.org/):
1) Do svého composer.json přidejte odkaz na privátní repozitář satis
```sh
"repositories": [
        {
            "type": "composer",
            "url": "https://satis.redenge.biz"
        }
]
```
2) Stáhněte balíček:
```sh
$ composer require redenge/engine
```

3) Do config.neon přidat extension:
```twig
extensions:
	- Redenge\Engine\DI\RedengeEngineExtension
```

4) Do config.neon aplikace přidat službu na vytváření rout:
```twig
services:
	routerFactory: Redenge\Engine\Router\RouterFactory
	router: @routerFactory::getRouter
```

5) V balíčku se nachází soubor bower.json, který si zkopírujte do DOCUMENT ROOTU projektu. Pokud již tento soubor v projektu máte, tak pouze doplňte závislosti a spusťte příkaz
```sh
$ bower install
```

6) V balíčku se nachází soubor package.json, který si zkopírujte do DOCUMENT ROOTU projektu. Pokud již tento soubor v projektu máte, tak pouze doplňte závislosti a spusťte příkaz
```sh
$ npm install
```

7) Ve složce ,,assets" se nachází složka ,,css". Obsah této složky zkopírujte do svého projektu do cesty {DOCUMENT_ROOT}/admin/css/src

8) Ve složce ,,assets" se nachází složka ,,js". Obsah této složky zkopírujte do svého projektu do cesty {DOCUMENT_ROOT}/admin/js/src

9) Je vyžadováno, aby styly a javascripty byly minifikované do jednoho souboru (styly zvlášť, javascripty zvlášť). V balíčku se proto nachází soubor gulpfile.js, který slouží pouze jako příklad, jak by mohl vypadat v konkrétním projektu.

- styly je nutné vygenerovat do {DOCUMENT_ROOT}/admin/css/app.min.css
- javascripty je nutné vygenerovat do {DOCUMENT_ROOT}/admin/js/app.min.js (kromě jquery, ta je automaticky přidána zvlášť)

10) Do ,,starého" konfigu aplikace (obvykle se nachází v etc/config.php) přidejte konstantu:
define('SETTINGS_THEME_COLUMN_NAME', 'theme');
Jako hodnotu konstanty napište název sloupce z tabulky ,,settings" - obvykle ,,theme" nebo ,,id_settings_theme"
