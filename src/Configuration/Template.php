<?php

namespace Redenge\Engine\Configuration;


/**
 * Description of Template
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class Template
{

	private $contentClass;


	public function __construct(array $configuration)
	{
		foreach ($configuration as $name => $value) {
			
			if (property_exists($this, $name) === false) {
				continue;
			}

			$this->{$name} = $value;
		}
	}


	/**
	 * @return string
	 */
	public function getContentClass()
	{
		return $this->contentClass ?: '';
	}

}
