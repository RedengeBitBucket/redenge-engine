<?php

namespace Redenge\Engine\Configuration;

/**
 * Description of Configuration
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class Configuration
{

	/**
	 * @var Template
	 */
	private $template;


	public function setTemplate(Template $template)
	{
		$this->template = $template;
	}


	/**
	 * @return Template
	 */
	public function getTemplate()
	{
		return $this->template;
	}

}
