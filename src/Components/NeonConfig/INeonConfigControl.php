<?php

namespace Redenge\Engine\Components\NeonConfig;

use Redenge\Engine\Entity\ISettings;


/**
 * Description of INeonConfigControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface INeonConfigControl
{

	/**
	 * @return NeonConfigControl
	 */
	function create(ISettings $settings, $filePath);

}
