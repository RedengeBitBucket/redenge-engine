<?php

namespace Redenge\Engine\Components\NeonConfig;

use Exception;
use Nette\Application\AbortException;
use Nette\Application\UI\Form;
use Nette\Database\Context;
use Nette\Database\Table\ActiveRow;
use Nette\Utils\FileSystem;
use Redenge\Engine\Components\BaseControl;
use Redenge\Engine\Entity\ISettings;


/**
 * Description of NeonConfigControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class NeonConfigControl extends BaseControl
{

	/**
	 * @var Context
	 */
	private $db;

	/**
	 * @var Entity\Settings
	 */
	private $settings;

	/**
	 * @var string
	 */
	private $filePath;

	/**
	 * @var null|callable
	 */
	public $onSuccess;

	/**
	 * @var null|callable
	 */
	public $onError;

	/**
	 * @var ActiveRow
	 */
	private $defaults;


	/**
	 * @param \Redenge\AdminModule\Entity\Settings $settings
	 * @param string $filePath
	 * @param Context $db
	 */
	public function __construct(ISettings $settings, $filePath, Context $db)
	{
		$this->db = $db;
		$this->settings = $settings;
		$this->filePath = $filePath;
	}


	/**
	 * @return Form
	 */
	public function createComponentForm()
	{
		$form = new Form;

		$form->addTextArea('setting')
			->setAttribute('class', 'syntax__neon form-control')
			->getLabelPrototype()->addClass('col-form-label');
		$form->addSubmit('save', 'Uložit')
			->setAttribute('class', 'btn btn-sm btn-success');

		$form->setDefaults($this->getDefaults());

		$form->onSuccess[] = [$this, 'formSuccess'];

		$renderer = $form->getRenderer();
		$renderer->wrappers['pair']['container'] = 'div class="form-group row"';
		$renderer->wrappers['label']['container'] = '';
		$renderer->wrappers['control']['container'] = 'div class="col-lg-12 col-md-12 col-sm-12"';

		return $form;
	}


	public function formSuccess(Form $form)
	{
		try {
			$setting = $form->values->setting;
			$this->settings->setSetting($setting);
			$data = $this->getDataArray();
			$id = isset($this->defaults->id) ? $this->defaults->id : null;
			if ($id) {
				$count = $this->db->table('settings')->where('id', $id)->update($data);
			} else {
				$this->db->table('settings')->insert($data);
			}
			$this->createConfig($setting);
			$this->onSuccess();
		} catch (AbortException $e) {
			throw $e;
		} catch (Exception $e) {
			$this->onError();
		}
	}


	/**
	 * @return array
	 */
	private function getDefaults()
	{
		$this->defaults = $this->db->table('settings')
			->where('code = ?', $this->settings->getCode())
			->fetch('setting');

		return $this->defaults === false ? [] : $this->defaults;
	}


	/**
	 * Vrátí pole pro uložení záznamů
	 * @todo - Lepší by bylo, aby se Entita sama starala o převedení na pole.
	 *
	 * @return array
	 */
	private function getDataArray()
	{
		$themeColumnName = defined('SETTINGS_THEME_COLUMN_NAME') ? SETTINGS_THEME_COLUMN_NAME : 'id_settings_theme';
		return [
			'code' => $this->settings->getCode(),
			'name' => $this->settings->getName(),
			'type' => 'textarea',
			'setting' => $this->settings->getSetting(),
			$themeColumnName => $this->settings->getIdSettingsTheme(),
		];
	}


	/**
	 * Vytvoří konfigurační config soubor na disku
	 *
	 * @param string $string
	 */
	private function createConfig($string)
	{
		$content = '# Datetime: ' . date('j.n.Y H:i:s') . PHP_EOL;
		$content .= $this->settings->getCode() . ':' . PHP_EOL . "\t";
		$content .= preg_replace("/((\r?\n)|(\r\n?))/", "\n\t", $string);

		$file = $this->filePath . '/' . $this->settings->getCode() . '.neon';
		FileSystem::write($file, $content);
	}

}
