<?php

namespace Redenge\Engine\Components\Tab;


/**
 * Description of ITabControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface ITabControl
{

	/**
	 * @param null|string       $breadcrumbStart
	 * @return TabControl
	 */
	function create($breadcrumbStart = null);

}
