<?php

namespace Redenge\Engine\Components\Tab;

use Nette\Application\UI\Control;
use Nette\Utils\Callback;
use Nette\Utils\Html;
use Nette\Utils\Strings;
use Redenge\Engine\Components\BaseControl;


class Helper extends BaseControl
{

	/**
	 * @var string
	 */
	private $_name;

	/**
	 * @var string
	 */
	private $code;

	/**
	 * @var array|Control[]|string[]|callable[]|mixed[]
	 */
	private $content = [];

	/**
	 * @var null|Html
	 */
	private $after;

	/**
	 * @var null|Html
	 */
	private $before;


	/**
	 * @param null|string $name
	 */
	public function __construct($name)
	{
		$this->_name = $name;
		$this->code = str_replace('-', '', Strings::webalize($name));
	}


	public function render()
	{
		$this->template->helper = $this;
		parent::render();
	}


	/**
	 * @param null|callable|mixed|Control|string    $content
	 */
	public function setContent($content)
	{
		$this->content = [$content];
	}


	/**
	 * @param null|callable|mixed|Control|string    $content
	 *
	 * @return self
	 */
	public function addContent($content)
	{
		$this->content[] = $content;

		return $this;
	}


	/**
	 * @param \Nette\Utils\Html $el
	 *
	 * @return self
	 */
	public function setAfter(Html $el)
	{
		$this->after = $el;

		return $this;
	}


	/**
	 * @return \Nette\Utils\Html|null
	 */
	public function getAfter()
	{
		return $this->after;
	}


	/**
	 *
	 * @param \Nette\Utils\Html $el
	 *
	 * @return self
	 */
	public function setBefore(Html $el)
	{
		$this->before = $el;

		return $this;
	}


	/**
	 * @return \Nette\Utils\Html|null
	 */
	public function getBefore()
	{
		return $this->before;
	}


	/**
	 * @param string $icon
	 *
	 * @return self
	 */
	public function setBeforeIcon($icon)
	{
		$el = Html::el('span');
		$el->class[] = 'fa';
		$el->class[] = 'fa-' . $icon;
		$el->style('margin-right: 3px');

		$this->setBefore($el);

		return $this;
	}


	/**
	 * @return string
	 */
	public function get_Name()
	{
		return $this->_name;
	}


	/**
	 * @return string
	 */
	public function getCode()
	{
		return $this->code;
	}


	/**
	 * @return null|mixed|Control|string
	 */
	public function getContent()
	{
		$realContent = [];
		foreach ($this->content as $content) {
			if (is_callable($content)) {
				$realContent[] = Callback::invokeArgs($content, [$this]);
			} else {
				$realContent[] = $content;
			}
		}

		return $this->content = $realContent;
	}

}
