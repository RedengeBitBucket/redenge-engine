<?php

namespace Redenge\Engine\Components\Tab;

use Redenge\Engine\Components\BaseControl;
use Tracy\Debugger;


class TabControl extends BaseControl
{

	/**
	 * @persistent
	 * @var string
	 */
	public $active;

	/**
	 * @var null|string
	 */
	private $breadcrumbStart;

	/**
	 * @var \ArrayIterator
	 */
	private $breadcrumb;

	/**
	 * @var null|callable
	 */
	public $onRender;


	/**
	 * @param  null|string      $breadcrumbStart
	 */
	public function __construct($breadcrumbStart = null)
	{
		$this->breadcrumbStart = $breadcrumbStart;
		$this->breadcrumb = new \ArrayIterator([]);
		$this->addComponent(new Tab($this), 'tab');
	}


	/**
	 * @return Tab
	 */
	public function getTab()
	{
		return $this['tab'];
	}


	/**
	 * {@inheritdoc}
	 */
	public function render()
	{
		if (is_callable($this->onRender)) {
			$this->onRender($this);
		}

		try {
			if ($this->active) {
				$components = explode('-', $this->active);
				$parent = $this;
				foreach ($components as $name) {
					if ($parent === null) {
						break;
					}

					$component = $parent->getComponent($name, false);
					if ($component instanceof Bookmark) {
						$component->setActive();
						$component->getContent();
						$this->addToBreadcrumb($component->get_Name());
					}
					$parent = $component;
				}
			}
		} catch (\Exception $e) {
			Debugger::log("Someone rewriting url :) '{$this->active}', exception: {$e->getMessage()}");
		}

		if ($this->breadcrumbStart) {
			$this->addBeforeBreadcrumb($this->breadcrumbStart);
		}
		$this->template->breadcrumb = $this->breadcrumb;

		parent::render();
	}


	/**
	 * @param string    $part
	 *
	 * @return self
	 */
	public function addToBreadcrumb($part)
	{
		$this->breadcrumb->append($part);

		return $this;
	}


	/**
	 * @param string    $part
	 *
	 * @return self
	 */
	public function addBeforeBreadcrumb($part)
	{
		$arr = array_merge([$part], $this->breadcrumb->getArrayCopy());
		$this->breadcrumb = new \ArrayIterator($arr);

		return $this;
	}

}
