<?php

namespace Redenge\Engine\Components\Tab;

use Nette;
use Redenge\Engine\Components\BaseControl;


class Tab extends BaseControl
{

	/**
	 * @var \Redenge\Engine\Components\Tab\TabControl
	 */
	private $control;


	/**
	 * @param \Redenge\Engine\Components\Tab\TabControl $control
	 */
	public function __construct(TabControl $control)
	{
		$this->control = $control;
	}


	/**
	 * @param string                                                        $name
	 * @param null|Nette\Application\UI\Control|string|callable|mixed       $content
	 *
	 * @return \Redenge\Engine\Components\Tab\Bookmark
	 * @throws \Exception
	 */
	public function addBookmark($name, $content = NULL)
	{
		$bookmark = new Bookmark($this->control, $name);
		$bookmark->setContent($content);
		$this->addComponent($bookmark, $bookmark->getCode());

		return $bookmark;
	}


	/**
	 * @param string                                                        $name
	 * @param null|Nette\Application\UI\Control|string|callable|mixed       $content
	 *
	 * @return \Redenge\Engine\Components\Tab\Helper
	 * @throws \Exception
	 */
	public function addHelper($name, $content = NULL)
	{
		$helper = new Helper($name);
		$helper->setContent($content);
		$this->addComponent($helper, $helper->getCode());

		return $helper;
	}


	/**
	 * {@inheritdoc}
	 */
	public function render()
	{
		$bookmarks = $this->getComponents(false, Bookmark::class);
		$hasActive = false;
		$first = '';
		$iterator = 0;
		foreach ($bookmarks as $key => $bookmark) {
			if ($iterator === 0) {
				$first = $key;
			}
			if ($bookmark->isActive()) {
				$hasActive = true;
				break;
			}
			$iterator++;
		}

		if (!$hasActive && $bookmarks->offsetExists($first)) {
			/** @var Bookmark $bookmark */
			$bookmark = $bookmarks->offsetGet($first);
			$bookmark->setActive();
			$this->control->addToBreadcrumb($bookmark->get_Name());
		}

		$this->template->bookmarks = $bookmarks;
		$this->template->helpers = $this->getComponents(FALSE, Helper::class);

		parent::render();
	}

}
