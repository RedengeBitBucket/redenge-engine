<?php

namespace Redenge\Engine\Components\Tab;

use Nette\Application\UI\Control;
use Nette\Utils\Callback;
use Nette\Utils\Html;
use Nette\Utils\Strings;
use Redenge\Engine\Components\BaseControl;


class Bookmark extends BaseControl
{

	/**
	 * @var \Redenge\Engine\Components\Tab\TabControl
	 */
	private $control;

	/**
	 * @var string
	 */
	private $_name;

	/**
	 * @var string
	 */
	private $code;

	/**
	 * @var array|Control[]|string[]|callable[]|mixed[]
	 */
	private $content = [];

	/**
	 * @var null|string
	 */
	private $link;

	/**
	 * @var bool
	 */
	private $active = false;

	/**
	 * @var string
	 */
	private $plink = 'this';

	/**
	 * @var null|Html
	 */
	private $after;

	/**
	 * @var null|Html
	 */
	private $before;


	/**
	 * @param \Redenge\Engine\Components\Tab\TabControl      $control
	 * @param null|string                                   $name
	 */
	public function __construct(TabControl $control, $name)
	{
		$this->control = $control;
		$this->_name = $name;
		$this->code = str_replace('-', '', Strings::webalize($name));
	}


	/**
	 * Bookmark content
	 */
	public function render()
	{
		$this->template->setFile(__DIR__ . '/templates/BookmarkContent.latte');
		$this->template->bookmark = $this;

		parent::render();
	}


	/**
	 * Bookmark top
	 */
	public function renderBookmark()
	{
		$this->template->setFile(__DIR__ . '/templates/Bookmark.latte');
		$this->template->bookmark = $this;

		parent::render();
	}


	/**
	 * @param string $code
	 */
	public function setCode($code)
	{
		$this->code = $code;
	}


	/**
	 * @param null|callable|mixed|Control|string    $content
	 */
	public function setContent($content)
	{
		$this->content = [$content];
	}


	/**
	 * @param null|callable|mixed|Control|string    $content
	 *
	 * @return self
	 */
	public function addContent($content)
	{
		$this->content[] = $content;

		return $this;
	}


	/**
	 * @param \Nette\Utils\Html $el
	 *
	 * @return self
	 */
	public function setAfter(Html $el)
	{
		$this->after = $el;

		return $this;
	}


	/**
	 * @return \Nette\Utils\Html|null
	 */
	public function getAfter()
	{
		return $this->after;
	}


	/**
	 *
	 * @param \Nette\Utils\Html $el
	 *
	 * @return self
	 */
	public function setBefore(Html $el)
	{
		$this->before = $el;

		return $this;
	}


	/**
	 * @return \Nette\Utils\Html|null
	 */
	public function getBefore()
	{
		return $this->before;
	}


	/**
	 * @param string $icon
	 *
	 * @return self
	 */
	public function setBeforeIcon($icon)
	{
		$el = Html::el('span');
		$el->class[] = 'fa';
		$el->class[] = 'fa-' . $icon;
		$el->style('margin-right: 3px');

		$this->setBefore($el);

		return $this;
	}


	/**
	 * @param bool $active
	 */
	public function setActive($active = true)
	{
		$this->active = $active;
	}


	/**
	 * @return \Redenge\Engine\Components\Tab\Tab
	 * @throws \Exception
	 */
	public function addTab()
	{
		$tab = new Tab($this->control);
		$this->addComponent($tab, 'tab');
		$this->content = $tab;

		return $tab;
	}


	/**
	 * @return string
	 */
	public function get_Name()
	{
		return $this->_name;
	}


	/**
	 * @return string
	 */
	public function getCode()
	{
		return $this->code;
	}


	/**
	 * @return null|mixed|Control|string
	 */
	public function getContent()
	{
		$realContent = [];
		foreach ($this->content as $content) {
			if (is_callable($content)) {
				$realContent[] = Callback::invokeArgs($content, [$this]);
			} else {
				$realContent[] = $content;
			}
		}

		return $this->content = $realContent;
	}


	/**
	 * @return string
	 */
	public function getLink()
	{
		if ($this->link) {
			return $this->link;
		}

		return $this->link = $this->control->presenter->link($this->plink, [
			"{$this->control->name}-active" => $this->lookupPath(TabControl::class, false)
		]);
	}


	/**
	 * @return bool
	 */
	public function isActive()
	{
		return $this->active;
	}


	/**
	 * @return bool
	 */
	public function hasTab()
	{
		return ($this->content instanceof Tab);
	}


	/**
	 * @param string $action
	 *
	 * @return self
	 */
	public function setAction($action)
	{
		$this->plink = $action;

		return $this;
	}


	/**
	 * @return string
	 */
	public function getAction()
	{
		return $this->plink;
	}


	/**
	 * @return TabControl
	 */
	public function getControl()
	{
		return $this->control;
	}

}
