<?php

namespace Redenge\Engine\Components\ModuleMenu;

use Redenge\Engine\Components\BaseControl;


final class ModuleMenuControl extends BaseControl
{

	const
		OLD_BASE_PATH = '/admin/index.php',
		MODULE_LINK_MAP = '/admin/v7/%s/%s/%s';

	/**
	 * @var string
	 */
	public $backlink = '';

	/**
	 * @var string
	 */
	protected $templateFileName = null;


	/**
	 * ModuleMenuControl constructor.
	 *
	 * @param string $backLink
	 */
	public function __construct($backLink = '')
	{
		$this->backlink = $backLink;
	}


	public function render()
	{
		$components = $this->getModules();
		$oldBasePath = self::OLD_BASE_PATH;
		foreach ($components as $component_name => $components) {
			if (array_key_exists('enable', $components) && $components['enable'] === false) {
				continue;
			}

			$_component_name = '';

			if ($_component_name === '' && $components['default']) {
				$_component_name = $component_name;
			}

			$modules = [];

			if ($component_name === $_component_name) {
				$tmp = $components['modules'];
				/*usort($tmp, function($a, $b) {
					return strcmp($a['caption'], $b['caption']);
				});*/

				foreach ($tmp as $module) {
					if ((array_key_exists('enable', $module) && $module['enable'] === false)) {
						continue;
					}
					if (isset($module['extension'])) {
						$parts = explode(':', $module['extension']);
						unset($parts[count($parts) - 1]);
					}

					$newModule = (object) [
						'name' => $module['caption'],
						'link' => isset($module['extension']) && $module['extension']
							? $this->getPresenter()->link($module['extension'])
							: sprintf("%s?c=%s&amp;m=%s", $oldBasePath, $component_name, $module["name"]),
						'active' => isset($module['extension']) && $module['extension']
							? $this->getPresenter()->isLinkCurrent(implode(':', $parts) . ':*')
							: FALSE,
					];

					$modules[] = $newModule;
				}
			}
		}

		$this->getTemplate()->modules = $modules;

		parent::render();
	}


	/**
	 * @return array
	 */
	public static function getModules()
	{
		return require ENGINE_PATH . '/etc/modules.php';
	}


	/**
	 * @deprecated
	 * @param strign $link Homepage:default etc.
	 * @return string
	 */
	public static function getModuleLink($link)
	{
		list($null, $module, $presenter, $action) = explode(':', $link);
		return sprintf(self::MODULE_LINK_MAP, $module, $presenter, $action);
	}

}
