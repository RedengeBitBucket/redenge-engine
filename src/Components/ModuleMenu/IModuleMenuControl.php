<?php

namespace Redenge\Engine\Components\ModuleMenu;


/**
 * Description of IModuleMenuControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface IModuleMenuControl
{

	/**
	 * @param string    $backLink
	 *
	 * @return ModuleMenuControl
	 */
	function create($backLink = '');

}
