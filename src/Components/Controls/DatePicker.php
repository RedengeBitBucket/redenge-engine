<?php

namespace Redenge\Engine\Components\Controls;

use Nette;
use Nette\Forms;
use Nette\Utils\Html;

/**
 * Form control for selecting date.
 *
 * @author   Jan Tvrdik
 * @author   Jan Skrasek
 */
class DatePicker extends DateTimePickerPrototype
{
	const CALL_REGISTER_METHOD = __CLASS__ . '::register();';

	/** @var string */
	protected $htmlFormat = 'd. m. Y H:i';
	/** @var string */
	protected $htmlType = 'text';

	protected function getDefaultParser()
	{
		return function($value) {
			if (!preg_match('#^(?P<dd>\d{1,2})[. -] *(?P<mm>\d{1,2})(?:[. -] *(?P<yyyy>\d{4})?)?(?: *[ @-] *(?P<hh>\d{1,2})[:.](?P<ii>\d{1,2})(?:[:.](?P<ss>\d{1,2}))?)?$#', $value, $matches)) {
				return null;
			}
			$dd = $matches['dd'];
			$mm = $matches['mm'];
			$yyyy = isset($matches['yyyy']) ? $matches['yyyy'] : date('Y');
			$hh = isset($matches['hh']) ? $matches['hh'] : 0;
			$ii = isset($matches['ii']) ? $matches['ii'] : 0;
			$ss = isset($matches['ss']) ? $matches['ss'] : 0;
			if (!($hh >= 0 && $hh < 24 && $ii >= 0 && $ii <= 59 && $ss >= 0 && $ss <= 59)) {
				$hh = $ii = $ss = 0;
			}
			if (!checkdate($mm, $dd, $yyyy)) {
				return null;
			}
			$value = new Nette\Utils\DateTime;
			$value->setDate($yyyy, $mm, $dd);
			$value->setTime($hh, $ii, $ss);
			return $value;
		};
	}

	public static function register()
	{
		Forms\Container::extensionMethod('addDatePicker', function (Forms\Container $container, $name, $label = NULL) {
			return $container[$name] = new DatePicker($label);
		});
	}

	/**
	 * @param \Nette\Utils\Html $control
	 *
	 * @return \Nette\Utils\Html
	 */
	protected function getCustomControl($control)
	{
		$div = Html::el('div class="input-group date datetimepicker"');
		if (!empty($this->pickerAttributes))
			$div->addAttributes($this->pickerAttributes);

		$label = Html::el('label class="input-group-addon"')->addAttributes(['for' => $control->id]);
		$label->addHtml(Html::el('i class="fa fa-calendar"'));
		$div->addHtml($control)
			->addHtml($label);

		return $div;
	}


}