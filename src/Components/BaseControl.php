<?php

namespace Redenge\Engine\Components;

use Nette\Application\AbortException;
use Nette\Application\UI\Control;
use Nette\Bridges\ApplicationLatte\Template;
use Nette\Localization\ITranslator;
use Nette\Utils\Strings;
use Redenge\Engine\Exception\MissingTemplateException;


abstract class BaseControl extends Control
{

	/**
	 * @var ITranslator
	 */
	protected $translator;

	/**
	 * @var null|string
	 */
	protected $path = null;

	/**
	 * @var bool
	 */
	private $render = true;


	/**
	 * @param \Nette\Localization\ITranslator $translator
	 */
	public function injectTranslator(ITranslator $translator)
	{
		$this->translator = $translator;
	}


	public function render()
	{
		if ($this->render) {
			$this->template->render();
		}
	}


	protected function createTemplate()
	{
		/** @var Template $template */
		$template = parent::createTemplate();
		$path = dirname($this->getReflection()->getFileName()) . '/templates/' 
			. Strings::firstLower($this->getReflection()->getShortName()) . '.latte';
		$pathUpper = dirname($this->getReflection()->getFileName()) . '/templates/'
			. Strings::firstUpper($this->getReflection()->getShortName()) . '.latte';

		if (!is_null($this->path)) {
			$tmp = $this->path;
		} elseif (is_file($path)) {
			$tmp = $path;
		} elseif (is_file($pathUpper)) {
			$tmp = $pathUpper;
		} else {
			throw new MissingTemplateException("Missing template for component {$this->getReflection()->getName()}.");
		}

		$template->setTranslator($this->translator);
		$template->setFile($tmp);

		return $template;
	}


	/**
	 * @param string
	 */
	public function setTemplatePath($path)
	{
		$this->path = $path;
	}


	/**
	 * @return string
	 */
	public function __toString()
	{
		$this->render = false;
		$this->render();

		return (string) $this->template;
	}


	/**
	 * @param null|string $snippet
	 * @throws AbortException
	 */
	public function redraw($snippet = NULL)
	{
		if ($this->presenter && $this->presenter->isAjax()) {
			$this->redrawControl($snippet);
		} else {
			$this->redirect('this');
		}
	}

}
