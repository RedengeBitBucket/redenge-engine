<?php

namespace Redenge\Engine\DI;

use Nette\Application\IPresenterFactory;
use Nette\DI\CompilerExtension;
use Nette\DI\Statement;
use Nette\PhpGenerator\ClassType;
use Redenge\Engine\Components\Controls\DatePicker;
use Redenge\Engine\Configuration\Configuration;
use Redenge\Engine\Configuration\Template;


/**
 * Description of RedengeEngineExtension
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class RedengeEngineExtension extends CompilerExtension
{

	/**
	 * {@inheritdoc}
	 */
	public function beforeCompile()
	{
		$builder = $this->getContainerBuilder();
		$builder->getDefinition($builder->getByType(IPresenterFactory::class))->addSetup(
			'setMapping',
			[['Engine' => 'Redenge\Engine\Presenters\*Presenter']]
		);

		if (method_exists($this->compiler, 'loadDefinitions')) {
			// Nette 2.4
			$this->compiler->loadDefinitions(
				$builder, $this->loadFromFile(__DIR__ . '/config.neon')['services'], $this->name
			);
		} else {
			// Nette 2.3
			$this->compiler->parseServices(
				$builder, $this->loadFromFile(__DIR__ . '/config.neon'), $this->name
			);
		}
	}


	/**
	 * {@inheritdoc}
	 */
	public function afterCompile(ClassType $class)
	{
		$initialize = $class->getMethod('initialize');
		$initialize->addBody(DatePicker::CALL_REGISTER_METHOD);
	}


	public function loadConfiguration()
	{
		parent::loadConfiguration();

		$config = $this->getConfig();

		$template = new Statement(Template::class, [
			'configuration' => isset($config['template']) ? $config['template'] : [] 
		]);

		$builder = $this->getContainerBuilder();
		$builder->addDefinition($this->prefix('configuration'))
			->setClass(Configuration::class)
			->addSetup('setTemplate', [$template]);
	}

}
