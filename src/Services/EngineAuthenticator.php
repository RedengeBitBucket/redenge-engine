<?php

namespace Redenge\Engine\Services;

use Nette\Database\Context;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;


/**
 * Description of Authenticator
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class EngineAuthenticator implements IAuthenticator
{

	/**
	 * @var Context
	 */
	protected $db;


	public function __construct(Context $db)
	{
		$this->db = $db;
	}


	/**
	 * {@inheritdoc}
	 */
	public function authenticate(array $data)
	{
		list ($username, $password, $multishopId) = $data;

		$row = $this->db->table('engine_user')->where('login = ? AND my_password = ?' . ($multishopId !== null ? ' AND multishop_id = ?' : ''),
			$username, md5($password), $multishopId
		)->fetch();

		if (!$row) {
            throw new AuthenticationException('Please log in');
        }

        return new Identity($row->id, null, ['login' => $row->login]);
	}

}
