<?php

namespace Redenge\Engine\Tools;

/**
 * Description of CategoryGroupHelper
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class CategoryGroupHelper
{

	public $id = 0;
	public $name = "";
	public $code = "";
	public $id_category = 0;
	public $children = array();
	public $hasimage = false;
	public $hastext = false;
	public $nodisplay = 0;
	private $specialListId;
	private $active;


	public function __construct($_id, $_id_category, $_code, $_name, $_hasimage, $_hastext, $_nodisplay)
	{
		$this->id = $_id;
		$this->name = $_name;
		$this->code = $_code;
		$this->id_category = $_id_category;
		$this->hasimage = $_hasimage;
		$this->hastext = $_hastext;
		$this->nodisplay = $_nodisplay;
	}


	/**
	 * @return mixed
	 */
	public function getActive()
	{
		return $this->active;
	}


	/**
	 * @param mixed $active
	 */
	public function setActive($active)
	{
		$this->active = $active;
	}

}
