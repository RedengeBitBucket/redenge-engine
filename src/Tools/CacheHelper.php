<?php

namespace Redenge\Engine\Tools;

use Nette\Utils\Finder;


/**
 * Description of CacheHelper
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class CacheHelper
{

	public static function removeConfigurator()
	{
		foreach (Finder::findFiles('*.*')->in(APP_DIR . '/temp/cache/Nette.Configurator') as $file) {
			unlink($file);
		}
	}

}
