<?php

namespace Redenge\Engine\Router;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


class RouterFactory
{

	/**
	 * @var RouteList
	 */
	private $router;


	public function __construct()
	{
		$this->router = new RouteList;
	}


	/**
	 * Přidá do routeru další pravidlo
	 * Slouží hlavně pro nové extensions, které potřebují přidat svá routovací pravidla
	 *
	 * @param  string  URL mask, e.g. '<presenter>/<action>/<id \d{1,3}>'
	 * @param  array|string|\Closure  default values or metadata or callback for NetteModule\MicroPresenter
	 * @param  int     flags
	 */
	public function append($mask, $metadata = [], $flags = 0)
	{
		$this->router[] = new Route($mask, $metadata, $flags);
	}


	/**
	 * @return Nette\Application\IRouter
	 */
	public function getRouter()
	{
		$this->router[] = new Route('v7[/<module>/<presenter>/<action>[/<id>]]', 'Admin:Dashboard:default');
		$this->router[] = new Route('[index.php]', 'Admin:Bridge:default');
		$this->router[] = new Route('[popup.php]', 'Admin:Bridge:popup');
		//$this->router[] = new Route('[<path .+>]', 'Admin:Bridge:default');
		return $this->router;
	}

}
