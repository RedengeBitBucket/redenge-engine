<?php

namespace Redenge\Engine\Entity;


/**
 * Description of Entity
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
abstract class Entity
{

	/**
	 * @var int
	 */
	private $id;


	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}


	/**
	 * @return ing
	 */
	public function getId()
	{
		return $this->id;
	}

}
