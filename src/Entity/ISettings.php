<?php

namespace Redenge\Engine\Entity;


/**
 * Description of ISettings
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface ISettings
{

	const TYPE_TEXT = 'text';
	const TYPE_TEXTAREA = 'textarea';

	/**
	 * @return string
	 */
	public function getCode();


	/**
	 * @return string
	 */
	public function getName();


	/**
	 * @return string
	 */
	public function getType();


	/**
	 * @return string
	 */
	public function getSetting();


	/**
	 * @return int
	 */
	public function getIdSettingsTheme();


	/**
	 * @param string $code
	 */
	public function setCode($code);


	/**
	 * @param string $name
	 */
	public function setName($name);


	/**
	 * @param string $type
	 */
	public function setType($type);


	/**
	 * @param string $setting
	 */
	public function setSetting($setting);


	/**
	 * @param int $idSettingsTheme
	 */
	public function setIdSettingsTheme($idSettingsTheme);

}
