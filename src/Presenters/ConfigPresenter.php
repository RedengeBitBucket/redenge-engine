<?php

namespace Redenge\Engine\Presenters;

use Redenge\Engine\Components\NeonConfig\INeonConfigControl;
use Redenge\Engine\Entity;
use Redenge\Engine\Presenters\BasePresenter;
use Redenge\Engine\Tools\CacheHelper;

/**
 * Description of EngineSettingsPresenter
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class ConfigPresenter extends BasePresenter
{

	/**
	 * @var INeonConfigControl @inject
	 */
	public $neonConfigFactory;


	/**
	 * @return \Redenge\Engine\Components\Tab\TabControl
	 */
	protected function createComponentMainTab()
	{
		$control = $this->tabControl->create('Engine');

		$main = $control->getTab();

		# Main
		$main->addBookmark('Konfigurace', function () {
			return $this['neonConfig'];
		})->setAction('Config:default');

		return $control;
	}


	public function createComponentNeonConfig()
	{
		$settings = (new Entity\Settings)
			->setCode('redengeEngine')
			->setName('Konfigurace engine')
			->setIdSettingsTheme(0);
		$filePath = APP_DIR . '/AdminModule/config/generated';

		$control = $this->neonConfigFactory->create($settings, $filePath);

		$control->onSuccess[] = function() {
			CacheHelper::removeConfigurator();
			$this->flashMessage('Hodnoty byly uloženy.', 'success');
			$this->redrawControl();
		};
		$control->onError[] = function() {
			$this->flashMessage('Během ukládaní nastala chyba.', 'danger');
			$this->redrawControl();
		};

		return $control;
	}

}
