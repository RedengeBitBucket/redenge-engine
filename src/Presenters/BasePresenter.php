<?php

namespace Redenge\Engine\Presenters;

use Nette\Application\UI\Presenter;
use Nette\DI\Container;
use Redenge\Engine\Components\ModuleMenu\IModuleMenuControl;
use Redenge\Engine\Components\Tab\ITabControl;
use Redenge\Engine\Configuration\Configuration;


abstract class BasePresenter extends Presenter
{

	/**
	 * @var IModuleMenuControl @inject
	 */
	public $moduleMenuControl;

	/**
	 * @var ITabControl @inject
	 */
	public $tabControl;

	/**
	 * @var Container
	 */
	private $serviceLocator;

	/**
	 * @var Configuration @inject
	 */
	public $configuration;



	/**
	 * @todo Check for better (lazy) solutions
	 * Public due BaseControl
	 * @param Container $serviceLocator
	 */
	public function injectContainer(Container $serviceLocator)
	{
		$this->serviceLocator = $serviceLocator;
	}


	/**
	 * @return Container
	 */
	public function getServiceLocator()
	{
		return $this->serviceLocator;
	}


	public function findLayoutTemplateFile()
	{
		if ($this->layout === FALSE) {
			return;
		}

		return __DIR__ . '/../templates/@layout.latte';
	}


	/**
	 * @return \Redenge\Engine\Components\ModuleMenu\ModuleMenuControl
	 */
	protected function createComponentModuleMenu()
	{
		$menu = $this->moduleMenuControl->create();

		return $menu;
	}


	public function beforeRender()
	{
		parent::beforeRender();

		$this->template->contentClass = $this->configuration->getTemplate()->getContentClass();
	}

}
