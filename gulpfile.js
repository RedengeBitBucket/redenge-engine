var gulp = require('gulp');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var cleanCSS = require('gulp-clean-css');
var clean = require('gulp-clean');
var jshint = require('gulp-jshint');

var admin_bases = {
	js_dist: 'admin/js/',
	css_dist: 'admin/css/'
};
var admin_paths = {
	scripts: [
		'bower_components/codemirror/lib/codemirror.js',
		'bower_components/codemirror/mode/yaml/yaml.js',
		'bower_components/codemirror/mode/css/css.js',
		'bower_components/codemirror/addon/edit/trailingspace.js',
		'bower_components/codemirror/addon/display/show-invisibles.js',
		'admin/js/src/**/*.js'
	],
	styles: [
		'bower_components/codemirror/lib/codemirror.css',
		'bower_components/codemirror/theme/monokai.css',
		'admin/css/src/**/*.css'
	]
};

gulp.task('admin_scripts_clean', function() {
	return gulp.src(admin_bases.js_dist + '*.js')
	.pipe(clean());
});
gulp.task('admin_scripts_dev', ['admin_scripts_clean'], function() {
	gulp.src(admin_paths.scripts)
	.pipe(jshint())
	.pipe(jshint.reporter('default'))
	.pipe(concat('app.min.js'))
	.pipe(gulp.dest(admin_bases.js_dist));
});
gulp.task('admin_scripts', ['admin_scripts_clean'], function() {
	gulp.src(admin_paths.scripts)
	.pipe(jshint())
	.pipe(jshint.reporter('default'))
	.pipe(uglify())
	.pipe(concat('app.min.js'))
	.pipe(gulp.dest(admin_bases.js_dist));
});
gulp.task('admin_styles', function() {	
	gulp.src(admin_paths.styles)
	.pipe(cleanCSS())
	.pipe(concat('app.min.css'))
	.pipe(gulp.dest(admin_bases.css_dist));
});
gulp.task('admin_watch', function() {
	gulp.watch(admin_paths.scripts, ['admin_scripts']);
	gulp.watch(admin_paths.styles, ['admin_styles']);
});
