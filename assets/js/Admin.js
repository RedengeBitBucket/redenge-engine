$(document).ready(function(){
	var admin = new Admin();
});

/**
 * [Admin - Main admin class]
 * @class Admin
 */
function Admin() {
	this.init();
	this.listener();
}

Admin.prototype.init = function() {
	var _this = this;

	this.initDatePicker();
	this.initTimePicker();

	$.nette.init();
};

Admin.prototype.listener = function() {

	var options = {
		theme: 'monokai',
		lineNumbers: true,
		indentWithTabs: true,
		showTrailingSpace: true,
		showInvisibles: true,
		viewportMargin: Infinity
	};

	$('.syntax__neon').each(function(index, elem) {
		CodeMirror.fromTextArea(elem, $.extend(options, {
			mode: 'yaml'
		}));
	});

	$('.syntax__css').each(function(index, elem) {
		CodeMirror.fromTextArea(elem, $.extend(options, {
			mode: 'css'
		}));
	});
};

Admin.prototype.initDatePicker = function() {
	$('.datetimepicker').each(function() {
        var $dateTimePicker = $(this);
        var defaultValue = $dateTimePicker.data('date');
        var format = $dateTimePicker.data('format');

        var $settings = {
            locale: 'cs',
            useCurrent: false,
            showClear: true
        };

        if (defaultValue && defaultValue != '0000-00-00')
            $settings['defaultDate'] = defaultValue;
        if (format)
            $settings['format'] = format;

        $dateTimePicker.datetimepicker($settings);
    });
};

Admin.prototype.initTimePicker = function() {
	$('.timepicker').each(function() {
        var $timePicker = $(this);
        var $input = $timePicker.children('input');
        var defaultValue = $timePicker.data('time');
        var $settings = {
            showMeridian: false,
            minuteStep: 5
        };
        $settings['defaultTime'] = (defaultValue) ? defaultValue: 'current';

        $input.timepicker($settings);
    });
};
